import {createStore} from 'redux';
import reducer from './reducers/deck';

export default createStore(reducer);